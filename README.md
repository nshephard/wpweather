# WPWeather


This is a set of scripts to scrape weather information from the Tw@ter account
[@WPWeather](https://twitter.com/WPWeather) which tweets daily updates of weather from the
[Weston Park Weather Station](https://www.museums-sheffield.org.uk/museums/weston-park/planning-a-visit/weston-park-weather-station)
located within the city of [Sheffield's](https://en.wikipedia.org/wiki/Sheffield) public
[Weston Park](https://en.wikipedia.org/wiki/Weston_Park%2C_Sheffield).  Another weather station in Sheffield that has
publicly available data can be viewed [here](https://meteostat.net/en/place/GB-AH8D).

![](img/rain.png)

For more plots please refer to the bottom of this document.

# Requirements

The code is written in [Python](https://www.python.org) 3 and uses a few common packages.

* [Pandas](https://pandas.pydata.org/)
* [Tweepy](https://www.tweepy.org/)

# Installation

Its recommended to install and use the package within its own dedicated
[Python Virtual Environment](https://realpython.com/python-virtual-environments-a-primer/).
Setting up and configuring such an environment is beyond the scope of this document, but the linked
primer should get you started.  The following instructions assume you are using the
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)

```bash
git clone
cd wpweather
mkvirtualenv wpweather

pip install .
```

# Usage

Firstly you'll need a [Twitter Development account](https://developer.twitter.com/) in order to use these scripts
as they access the [Twitter API](https://developer.twitter.com/en/docs/tweets/search/overview).

You should find that Python dependencies are pulled in during the installation.  You can run the code
from a [Jupyter Notebook](https://jupyter.org/) and an example is provided under
[notebooks](notebooks/example.ipynb).

Alternatively the code can be run as a script with a simple [YAML](https://yaml.org/) configuration file provided.
A sample configuration file is shown below, note that it refers to a file that contains the Twitter credentials,
the structure of which is shown below, and the values for which you will have to obtain from your own
[Twitter Development account](https://developer.twitter.com/).


```yaml
credentials: config/twitter_credentials.json
account: wpweather
outdir: tmp
remove_old: True
```

```json
{
    "ACCESS_KEY": "###############",
    "ACCESS_SECRET": "###############",
    "CONSUMER_KEY": "###############",
    "CONSUMER_SECRET": "###############"
}
```

You can then use the included `run.py` to run this and all output will be saved in the directory specified in the
configuration file (providing it exists).

```bash
python ./run.py -c config/config.yml
```

# Time-series plots

![](img/temp.png)
![](img/pressure.png)
![](img/relative_humidity.png)
![](img/sunshine.png)

# Links

Like most people I'm not that original and have used code snippets from others in developing this package.

* [@WPWeather](https://twitter.com/WPWeather)
* [How to Scrape Data from Twitter Using Python](https://www.promptcloud.com/blog/scrape-twitter-data-using-python-r/)
* [Twitter API](https://developer.twitter.com/en/docs/tweets/search/overview)
* [Oikiolab Weather Downloader](https://weatherdownloader.oikolab.com/downloader)