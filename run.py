"""
Generate all graphs.
"""
import logging
import argparse as arg

from wpweather.wpweather_config import WPweatherConfig
from wpweather.scrape_tweets import ScrapeTweets
from wpweather.extract_weather import ExtractWeather
from wpweather.summarise import Summarise

LOGGER = logging.getLogger('wpweather')


def create_parser():
    """
    Get arguments
    """
    parser = arg.ArgumentParser()
    parser.add_argument('-c', '--config_loc',
                        dest='config_loc',
                        required=False,
                        default='config/wpweather.yml')
    return parser


def main(config_file) -> None:
    """
    Get all tweets, parse, summarise and plot.
    """
    config = WPweatherConfig(config_file.config_loc).options

    tweets = ScrapeTweets(config['credentials'],
                          config['account'],
                          config['outdir'])
    tweets.scrape_data()

    weather = ExtractWeather(tweets.outtweets, remove_old=config['remove_old'])
    weather.extract_weather_tweets()

    summarise = Summarise(weather.weather_df, outdir=config['outdir'])
    # for x in weather.weather_df.columns:
    #     print(x)
    #     summarise.plot(x)
    summarise.plotnine('last_24_min_temp')
    summarise.plotnine('last_24_max_temp')
    summarise.plotnine('pressure')
    summarise.plotnine('rain')
    summarise.plotnine('relative_humidity')
    summarise.plotnine('sunshine')
    summarise.plotnine('temp')
    # summarise.plotnine(['last_24_min_temp', 'temp', 'last_24_max_temp'])
    summarise.summarise()
    summarise.summarise(groupby='week')
    summarise.summarise(groupby='month')
    summarise.summarise(groupby='year')


if __name__ == '__main__':
    PARSER = create_parser()
    ARGS = PARSER.parse_args()
    main(ARGS)
