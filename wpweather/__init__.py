"""
Initialise the logger
"""
import logging

LOG_FORMATTER = logging.Formatter(
    '%(asctime)s [%(levelname)s][%(name)s] %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
LOG_ERR_FORMATTER = logging.Formatter(
    '%(asctime)s [%(levelname)s][%(name)s][%(filename)s:%(lineno)d] %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
LOGGER_NAME = 'wpweather'
