"""
Summarise the data.
"""
from pathlib import Path
import logging

import pandas as pd
import matplotlib.pyplot as plt
from plotnine import ggplot, aes, element_text, geom_point, geom_smooth, xlab, ylab, ggtitle, theme, theme_xkcd

LOGGER = logging.getLogger('summarise_tweets')


class Summarise:
    """
    Summarise and plot the weather data
    """
    def __init__(self,
                 data: pd.DataFrame,
                 outdir: str,
                 figsize: tuple = (16, 9)):
        """
        Initialise the class.

        Parameters
        ----------

        data: Pandas DataFrame
            Dataframe of tidied data to plot.
        outdir: str
            Path to where output should be saved.
        figsize: tuple
            Dimensions of figure.
        """
        self.data = data
        self.outdir = Path(outdir)
        self.figsize = figsize
        self.summary = None
        self.ylabel = None
        self.data.to_csv(self.outdir / 'wpweather_clean.csv')

    @staticmethod
    def set_y_axis_label(to_plot: str):
        """
        Set the y-axis label
        """
        if isinstance(to_plot, list) or (to_plot == 'temp'):
            title = 'Temperature'
            ylabel = title + ' (Celsius)'
        elif to_plot == 'last_24_min_temp':
            title = 'Minimum 24hr Temperature'
            ylabel = title + ' (Celsius)'
        elif to_plot == 'last_24_max_temp':
            title = 'Maximum 24hr Temperature'
            ylabel = title + ' (Celsius)'
        elif to_plot == 'rain':
            title = 'Rain'
            ylabel = title + ' (mm)'
        elif to_plot == 'pressure':
            title = 'Pressure'
            ylabel = title + ' (hPa)'
        elif to_plot == 'sunshine':
            title = 'Sunshine'
            ylabel = title + ' (hrs)'
        elif to_plot == 'relative_humidity':
            title = 'Relative Humidity'
            ylabel = title + ' (%)'
        return title, ylabel

    def plot(self, to_plot: str):
        """
        Plot a time-series of the data.

        Parameters
        ----------

        to_plot: str
            String of variable to plot
        groupby: str
            Time period to group summarise data by before plotting.
        groupby_method:
            Type of summary statistic to summarise data by (e.g. mean|median|max|min)
        """
        title, ylabel = self.set_y_axis_label(to_plot)
        if isinstance(to_plot, list):
            plot_file = 'temperature' + '.png'
        else:
            plot_file = to_plot.lower() + '.png'

        # if groupby is not None:
        #     plot_df = self.summarise(groupby=groupby)
        #     plot_file = plot_file.replace('.png', '_' + groupby + '.png')
        # else:
        #     plot_df = self.data[to_plot]

        # plot = plot_df.plot(figsize=self.figsize,
        #                     title=title)

        plot = self.data[to_plot].plot(figsize=self.figsize, title=title)

        plot.set_ylabel(ylabel)
        plt.grid(True)
        fig = plot.get_figure()
        fig.savefig(self.outdir / plot_file)
        plt.close()
        LOGGER.info(f'Plot saved to : {self.outdir / plot_file}')

    def plotnine(self, to_plot: str):
        """
        Plot a time-series of the data.

        Parameters
        ----------

        to_plot: str
            String of variable to plot
        groupby: str
            Time period to group summarise data by before plotting.
        groupby_method:
            Type of summary statistic to summarise data by (e.g. mean|median|max|min)
        """
        title, ylabel = self.set_y_axis_label(to_plot)
        if isinstance(to_plot, list):
            plot_file = 'temperature' + '.png'
        else:
            plot_file = to_plot.lower() + '.png'

        # if groupby is not None:
        #     plot_df = self.summarise(groupby=groupby)
        #     plot_file = plot_file.replace('.png', '_' + groupby + '.png')
        # else:
        #     plot_df = self.data[to_plot]

        # plot = plot_df.plot(figsize=self.figsize,
        #                     title=title)

        plot = (ggplot(self.data, aes(self.data.index, to_plot)) +
                geom_point() + geom_smooth(stat='smooth', span=0.3) +
                xlab('Date') + ylab(ylabel) + ggtitle(title) +
                theme(axis_text_x=element_text(rotation=90, hjust=1)) +
                theme_xkcd())
        plot.save(filename=self.outdir / plot_file,
                  width=self.figsize[0],
                  height=self.figsize[1])
        LOGGER.info(f'Plot saved to : {self.outdir / plot_file}')

    def summarise(self, groupby: str = None):
        """
        Summarise the data.

        Parameters
        ----------

        groupby : str
            Grouping period to summarise data by (week|month|year).
        """
        if groupby is None:
            self.summary = self.data.describe()
        else:
            to_summarise = self.data.reset_index()
            if groupby == 'week':
                to_summarise['year'] = to_summarise['date'].dt.year
                to_summarise['week'] = to_summarise['date'].dt.week
                groupby = ['year', 'week']
            elif groupby == 'month':
                to_summarise['year'] = to_summarise['date'].dt.year
                to_summarise['month'] = to_summarise['date'].dt.month
                groupby = ['year', 'month']
            elif groupby == 'year':
                to_summarise['year'] = to_summarise['date'].dt.year

            self.summary = to_summarise.groupby(groupby).describe()
        self.summary_to_html(groupby)

    def summary_to_html(self, groupby: str):
        """
        Write the Summary to HTML file.

        Parameters
        ----------

        groupby: str
            Grouping to summarise by.
        """
        if groupby is None:
            outfile = 'summary.html'
        elif groupby == 'year':
            outfile = 'summary_' + groupby + '.html'
        else:
            outfile = 'summary_' + '_'.join(groupby) + '.html'
        with open(self.outdir / outfile, 'w', encoding='utf-8') as html:
            html.write(self.summary.to_html())
        LOGGER.info(
            f'HTMl summary grouped by {groupby} written to : {self.outdir / outfile}'
        )
