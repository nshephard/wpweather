"""
Scrape Tweets
"""
# From https://www.promptcloud.com/blog/scrape-twitter-data-using-python-r/
import csv
import json
import logging
from pathlib import Path

import tweepy
import pandas as pd

LOGGER = logging.getLogger('scrape_tweets')

# pylint: disable=too-many-format-args
# pylint: disable=too-many-instance-attributes


class ScrapeTweets:
    """
    Retrieve tweets from @WPWeather
    """
    def __init__(self, twitter_credentials, screen_name: str,
                 outdir: str) -> None:
        """
        Initialise the class.
        """
        self.outdir = Path(outdir)
        self.screen_name = screen_name
        with open(twitter_credentials, encoding='utf-8') as cred_data:
            info = json.load(cred_data)
        self.credentials = {
            'consumer_key': info['CONSUMER_KEY'],
            'consumer_secret': info['CONSUMER_SECRET'],
            'access_key': info['ACCESS_KEY'],
            'access_secret': info['ACCESS_SECRET']
        }
        self.timeline = None
        self.api = None
        self.all_the_tweets = []
        self.outtweets = None
        self.tweets_df = None

    def scrape_data(self) -> None:
        """
        Run all steps
        """
        self.authorise()
        self.get_timeline()
        self.get_all_tweets(self.timeline[-1].id - 1)
        self.extract_tweets()
        self.create_df()
        self.write_tweets()

    def authorise(self):
        """
        Get tweets from the account
        """
        # Twitter allows access to only 3240 tweets via this method
        # Authorization and initialization
        auth = tweepy.OAuthHandler(self.credentials['consumer_key'],
                                   self.credentials['consumer_secret'])
        auth.set_access_token(self.credentials['access_key'],
                              self.credentials['access_secret'])
        self.api = tweepy.API(auth)
        LOGGER.info('Successfully authenticated against Twitter API.')

    def get_timeline(self):
        """
        Get the timeline from Twitter.
        """
        self.timeline = self.api.user_timeline(screen_name=self.screen_name,
                                               count=200)
        LOGGER.info('Scraped the first 200 tweets.')

    def get_all_tweets(self, oldest_tweet):
        """
        Retrieve tweets.

        This method has a limit of 3240, consider alternative, date limited options
        in future.
        """
        self.all_the_tweets.extend(self.timeline)
        new_tweets = self.timeline
        while len(new_tweets) > 0:
            # The max_id param will be used subsequently to prevent duplicates
            new_tweets = self.api.user_timeline(screen_name=self.screen_name,
                                                count=200,
                                                max_id=oldest_tweet)
            self.all_the_tweets.extend(new_tweets)

            # id is updated to oldest tweet - 1 to keep track
            oldest_tweet = self.all_the_tweets[-1].id - 1
            LOGGER.info(
                f'...{len(self.all_the_tweets)} tweets have been downloaded so far'
            )

    def extract_tweets(self):
        """
        Extract tweets components to a list.
        """
        # transforming the tweets into a 2D array that will be used to populate the csv
        self.outtweets = [[
            tweet.id_str, tweet.created_at,
            tweet.text.encode('utf-8')
        ] for tweet in self.all_the_tweets]
        LOGGER.info('Extracted tweets.')

    def create_df(self):
        """
        Create a dataframe from the tweets.
        """
        self.tweets_df = pd.DataFrame(self.outtweets,
                                      columns=['id', 'date', 'text'])
        LOGGER.info('Tweets converted to Pandas dataframe')

    def write_tweets(self):
        """
        Write to CSV
        """
        outfile = self.screen_name + '_all_tweets.csv'
        with open(self.outdir / outfile, 'w', encoding='utf-8') as file:
            writer = csv.writer(file)
            writer.writerow(['id', 'date', 'text'])
            writer.writerows(self.outtweets)
        LOGGER.info('Tweets written to : {self.outdir / outfile}')
