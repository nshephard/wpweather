"""
Parse the configuration file.
"""
import logging
from pathlib import Path
from ruamel.yaml import YAML

LOGGER = logging.getLogger('wpweather_config')

# pylint: disable=too-few-public-methods


class WPweatherConfig:
    """
    Class for parsing the configuration file.
    """
    def __init__(self, config_file):
        """
        Initialise the class.
        """
        yaml = YAML(typ='safe')
        self.options = yaml.load(Path(config_file))
        LOGGER.info(f'Configuration loaded from file : {config_file}')
