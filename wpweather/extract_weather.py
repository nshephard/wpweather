"""
Extract the data from weather tweets.
"""
import logging
from typing import Union

import pandas as pd

LOGGER = logging.getLogger('extract_tweets')

# pylint: disable=too-few-public-methods


class ExtractWeather:
    """
    Extracts the data from weather tweets.
    """
    def __init__(self,
                 tweets: Union[str, list, pd.DataFrame],
                 remove_old: bool = True):
        """
        Initialise the class.

        Parameter
        ---------

        tweets:
            Tweets to be parsed, should either be a path to CSV, a list of tweets or a Pandas
            Dataframe of tweets.
        """
        if isinstance(tweets, str):
            self.all_tweets = pd.read_csv(tweets, encoding='utf-8')
        elif isinstance(tweets, pd.DataFrame):
            self.all_tweets = tweets
        else:
            self.all_tweets = pd.DataFrame(tweets,
                                           columns=['id', 'date', 'text'])
        self.remove_old = remove_old
        self.weather_tweets = None
        self.expanded_tweets = None
        self.weather_df = None
        self.extract_weather_tweets()

    def extract_weather_tweets(self):
        """
        Extract the tweets.
        """
        self.all_tweets = self._tidy_data_classes(self.all_tweets)
        self.weather_tweets = self._drop_non_weather(self.all_tweets)
        # Remove the last row
        self.weather_tweets = self.weather_tweets.drop(
            self.weather_tweets.tail(1).index)
        self.weather_tweets['text'] = self._remove_start(
            self.weather_tweets['text'])
        self.expanded_tweets = self._split_tweets(self.weather_tweets['text'])
        self.weather_df = self._extract_data_to_df(self.expanded_tweets)
        self.weather_df = self.weather_df.set_index(
            self.weather_tweets['date'])
        for var in self.weather_df.columns:
            self.weather_df[var] = self._tidy_text(self.weather_df[var])
        if self.remove_old:
            self.weather_df = self.weather_df.loc[
                self.weather_df.index.year > 2013]

    @staticmethod
    def _tidy_data_classes(weather_df: pd.DataFrame):
        """
        Ensure dtype of columns is correct.
        """
        weather_df = weather_df.infer_objects()
        weather_df['id'] = pd.to_numeric(weather_df['id'])
        weather_df['text'] = weather_df['text'].astype('str')
        return weather_df

    @staticmethod
    def _drop_non_weather(weather_df: pd.DataFrame):
        """
        Drop all tweets that do not begin with 'Weather at Weston Park'.
        """
        LOGGER.info('Removing non-weather tweets.')
        return weather_df[weather_df['text'].str.contains('Weather at')][[
            'date', 'text'
        ]]

    @staticmethod
    def _remove_start(text: str):
        """
        Remove common text from the start of tweets.
        """
        text = text.str.replace(r"b'Weather at \d+UTC on \d+/\d+/\d+: ", '')
        LOGGER.info('Removing leading text from tweets.')
        return text.str.replace(
            r"b'Weather at Weston Park on \d+/\d+/\d+: \d+ ", '')

    @staticmethod
    def _tidy_text(text):
        """
        Tidy the text.
        """
        text = text.str.replace(r'(^-?\d+(?:\.\d+)?)(.*)', r'\1', regex=True)
        LOGGER.info('Tidying text.')
        return pd.to_numeric(text, errors='coerce')

    @staticmethod
    def _split_tweets(text):
        """
        Split the text.
        """
        LOGGER.info('Splitting text.')
        return text.str.split(' ', expand=True)

    @staticmethod
    def _extract_data_to_df(data) -> pd.DataFrame:
        """
        Extract the data to a Pandas DataFrame.
        """
        temp = data[data[0] == 'Temp'][1]
        pressure = data[data[2] == 'Pressure'][3]
        relative_humidity = data[data[4] == 'RH'][5]
        last_24_max_temp = pd.concat(
            [data[data[9] == 'Max'][10], data[data[6] == 'Max'][7]], axis=0)
        last_24_min_temp = pd.concat(
            [data[data[11] == 'Min'][12], data[data[8] == 'Min'][9]], axis=0)
        rain = pd.concat(
            [data[data[13] == 'Rain'][14], data[data[10] == 'Rainfall'][11]],
            axis=0)
        sunshine = pd.concat(
            [data[data[15] == 'Sun'][16], data[data[12] == 'Sunshine'][13]],
            axis=0)

        weather = pd.DataFrame({
            'temp': temp,
            'pressure': pressure,
            'relative_humidity': relative_humidity,
            'last_24_max_temp': last_24_max_temp,
            'last_24_min_temp': last_24_min_temp,
            'rain': rain,
            'sunshine': sunshine,
        })
        LOGGER.info('Data frame created.')
        return weather
