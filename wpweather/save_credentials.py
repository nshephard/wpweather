"""
Save twitter credentials to a pickle for reuse.
"""
import json

# create a dictionary to store your twitter credentials

TWITTER_CRED = {}

# Enter your own consumer_key, consumer_secret, access_key and access_secret
# Replacing the stars ("********")

TWITTER_CRED['CONSUMER_KEY'] = ''
TWITTER_CRED['CONSUMER_SECRET'] = ''
TWITTER_CRED['ACCESS_KEY'] = ''
TWITTER_CRED['ACCESS_SECRET'] = ''

# Save the information to a json so that it can be reused in code without exposing
# the secret info to public

with open('twitter_credentials.json', 'w', encoding='utf-8') as secret_info:
    json.dump(TWITTER_CRED, secret_info, indent=4, sort_keys=True)
