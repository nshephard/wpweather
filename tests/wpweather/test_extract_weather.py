"""
Test of extracting weather.
"""
# import pytest


def test_tidy_data_classes():
    """
    Test conversion of data types
    """
    assert True


def test_drop_non_weather():
    """
    Test dropping non-weather tweets.
    """
    assert True


def test_remove_start():
    """
    Test stripping of common text from start of tweets.
    """
    assert True


def test_tidy_text():
    """
    Test tidying of text.
    """
    assert True


def test_split_tweets():
    """
    Test splitting tweets.
    """
    assert True


def test_extract_data_to_df():
    """
    Test extraction of data to Pandas DataFrame.
    """
    assert True
