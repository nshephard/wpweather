"""
Setup tests.
"""
import pandas as pd
import pytest

WEATHER = {'date': [],
           'temp': []}


# @pytest.fixture
# def mock_tweets():
#     """
#     Mock tweets structure.
#     """
#     pass


@pytest.fixture
def weather_df():
    """
    Make a mock weather data frame available.
    """
    return pd.DataFrame(WEATHER, index='date')
